#include <msp430.h> 
unsigned char distance=0;
unsigned char photo_resistance_1=0;
unsigned char photo_resistance_2=0;
unsigned char diff_lumiere=0;


void ADC_Demarrer_conversion(unsigned char voie)
{
     ADC10CTL1 = (voie * 0x1000)+ ADC10DIV_0 + ADC10SSEL_2 +  SHS_0 + CONSEQ_0 ;
    ADC10CTL0 |= ENC + ADC10SC;     // Sampling and conversion start
 }


int ADC_Lire_resultat ()
{
  	while (ADC10CTL1 & ADC10BUSY);	// Tant que ADC occup� on attend
	ADC10CTL0 &= ~ENC;		// Conversion finie alors Disable ADC conversion

    	return ADC10MEM;	        // Return Conversion value
}


void Avancer()
{
	// Avance tout droit
		    P2OUT |= BIT1; // à 1
		    P2OUT &= ~BIT5; // à 0
}

void TournerDroite()
{
			// tourner droit
		    P2OUT &= ~BIT1; // à 1
		    P2OUT &= ~BIT5; // à 0
}
void TournerGauche()
{
			// tout gauche
		    P2OUT |= BIT1; // à 1
		    P2OUT |= BIT5; // à 0

}



void ADC_init(void)
{
	  ADC10CTL0 = ADC10CTL1 = 0;

// Choix de la r�f�rence de tension Vcc GND
// R�f�rence interne active et g�n�rateur � 2,5 Volts  ADC10 actif
// Les autres bits sont suppos�s � 0

	  ADC10CTL0 =  SREF_0 + ADC10SHT_0  + REF2_5V + REFON + ADC10ON;  ;

// Choix du diviseur par 1 pour l'horloge, d�marrage conversion logiciel
// Horloge de conversion 1MHz, conversion monovoie-monocoup

	  ADC10CTL1 =  ADC10DIV_0 + ADC10SSEL_2 +  SHS_0 + CONSEQ_0 ;

}


void robot_init(void)
{
	P2DIR |= BIT1|BIT5; // Sens des moteurs en SORTIE

	    BCSCTL1 = CALBC1_1MHZ;  // Horloge
	    DCOCTL = CALDCO_1MHZ;

	    P2DIR |= BIT2|BIT4;  // PWM des moteurs en SORTIE

	    // gestion du multiplexage en mode FONTION PRIMAIRE
	    P2SEL |= BIT2 | BIT4;    // à 1
	    P2SEL2 &= ~(BIT2|BIT4);  // à 0

	    // Configuration du Timer
	    // TA1CTL = source horloge SMCLK (1MHZ) | Comptage en mode up | Prediviseur 1
	    TA1CTL = TASSEL_2 | MC_1 | ID_0;

	    // Activation mot de sortie 7
	    TA1CCTL1 = OUTMOD_7;
	    TA1CCTL2 = OUTMOD_7;

	    // 0,2ms = 1 / T
	    // Determine la periode du signal
	    TA1CCR0 = 200;

	    // sens des roue
	    // Avance tout droit
	   /* P2OUT |= BIT1; // à 1
	    P2OUT &= ~BIT5; // à 0
	    */
	    Avancer();

	    // detremine le rapport cylcique des signaux des moteurs ( ici 100% )
	    TA1CCR1 = 200;
	    TA1CCR2 = 195;

	   // ligne 5 du port 1 en entree
	    P1DIR &= ~BIT5;

	    // MODIF 20.11
			// front
			P1IES |= BIT5;

			// interruption sur le BIT3
			P1IE |= BIT5;

			// flag a zero
			P1IFG &= ~BIT5;

		P1DIR &= ~BIT7;
		P1DIR &= ~BIT4;
		P1DIR &= ~BIT6;
	// activation des interruptions
					//	__enable_interrupt();


			//init
		/*	ADC10CTL0 = ADC10CTL1 = 0;
			ADC10CTL0 =  SREF_0 + ADC10SHT_0  + REF2_5V + REFON + ADC10ON;  ;
			ADC10CTL1 =  ADC10DIV_0 + ADC10SSEL_2 +  SHS_0 + CONSEQ_0 ;
			///
			ADC10CTL1 = ('0x0010' * 0x1000)+ ADC10DIV_0 + ADC10SSEL_2 +  SHS_0 + CONSEQ_0 ;
			ADC10CTL0 |= ENC + ADC10SC;
			//

			while(1)
			{
				while (ADC10CTL1 & ADC10BUSY);	// Tant que ADC occup� on attend
							ADC10CTL0 &= ~ENC;
			}*/

}

void directionLumiere()
{
	if (photo_resistance_1 < photo_resistance_2)
		{
			// Tourne � droite
			TournerDroite();
			//__delay_cycles(5000);

		}
		else if (photo_resistance_1 > photo_resistance_2)
		{
			// Tourne � gauche
			TournerGauche();
			//__delay_cycles(5000);
		}

	else
	{

	}
}



/*
 * main.c
 */

int main(void) {



	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    // appel de la fonction int_robot() declare plus haut
    robot_init();
	// DEBUT MODIF 20.11 matin
	ADC_init();



	while(1)
		{
		Avancer();

		ADC_Demarrer_conversion(0x0010); // gauche
			photo_resistance_1=ADC_Lire_resultat (); // > 40 lumiere presente

			ADC_Demarrer_conversion(0x0040);
			photo_resistance_2=ADC_Lire_resultat (); // > 40 lumiere presente

			diff_lumiere=photo_resistance_1-photo_resistance_2;
			if(diff_lumiere > 15)
			{
				directionLumiere();
			}


			Avancer();
				__enable_interrupt();


			//P2OUT |= BIT1; // � 1
		//	P2OUT &= ~BIT5; // � 1


			// MODIF 20.11 aprem
				// test pour photo resistance
				/*ADC_Demarrer_conversion(0x0007);
				photo_resistance_1=ADC_Lire_resultat ();

				ADC_Demarrer_conversion(0x0006);
				photo_resistance_2=ADC_Lire_resultat ();

				diff_lumiere=photo_resistance_1-photo_resistance_2;
				if (diff_lumiere < 0)
				{
					diff_lumiere=photo_resistance_2-photo_resistance_1;
				}

				if(diff_lumiere > 30)
				{
					if (photo_resistance_1 < photo_resistance_2)
					{
						// Tourne � doite
						 P2OUT &= ~BIT1; // � 1
						 P2OUT &= ~BIT5; // � 1
						__delay_cycles(50000);
					}
					else if (photo_resistance_1 > photo_resistance_2)
					{
						// Tourne � gauche
						P2OUT |= BIT1; // � 0
						P2OUT |= BIT5; // � 0
						__delay_cycles(50000);
					}
				}
				else
				{
					TA1CCR1 = 200;
					TA1CCR2 = 200;
				}*/
			// FIN MODIF 20.11 aprem

		}

	// FIN  MODIF 20.11 matin

}

#pragma vector = PORT1_VECTOR
__interrupt void capteur_distance(void)
{
	/*while (ADC10CTL1 & ADC10BUSY);	// Tant que ADC occup� on attend
	{		ADC10CTL0 &= ~ENC;		// Conversion finie alors Disable ADC conversion
		if (ADC10MEM > 500)
		{
			  TA1CCR1 = 0;
			   TA1CCR2 = 0;
		}
		else
		{
			   TA1CCR1 = 200;
			   TA1CCR2 = 200;
		}*/

		/*	while ( ADC10MEM > 10 )
			{
				TA1CCR1 = 0;
				TA1CCR2 = 0;
			}
			TA1CCR1 = 50;
			TA1CCR2 = 200;*/
			//ADC10MEM = "0x0000";


	/*while (distance > 88)
	{
		TA1CCR1 = 0;
		TA1CCR2 = 0;
		ADC_Demarrer_conversion('0x0010');
		distance=ADC_Lire_resultat ();
	}

	if (distance <= 88)
	{
		TA1CCR1 = 200;
		TA1CCR2 = 200;
	}
	*/
	// lecture sur P1.5 (capteur de distance)


		  ADC_Demarrer_conversion(0x0005);
				distance=ADC_Lire_resultat ();

			if (distance > 150)
				{
					TA1CCR1 = 0;
					TA1CCR2 = 0;
				}
				else
				{
					TA1CCR1 = 200;
					TA1CCR2 = 195;
				}
	P1IFG &= ~BIT5;

}


